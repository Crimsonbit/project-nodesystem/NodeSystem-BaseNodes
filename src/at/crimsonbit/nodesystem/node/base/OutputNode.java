package at.crimsonbit.nodesystem.node.base;

import at.crimsonbit.nodesystem.nodebackend.annotations.NodeInput;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeOutput;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;

public class OutputNode extends AbstractNode {

	@NodeInput()
	Object input;

	@NodeOutput("compute")
	Object output;

	@Override
	public void compute() {
		output = input;
	}

}
