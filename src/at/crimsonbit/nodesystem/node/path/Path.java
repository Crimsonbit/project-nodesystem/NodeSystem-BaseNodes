package at.crimsonbit.nodesystem.node.path;

import at.crimsonbit.nodesystem.nodebackend.INodeType;

public enum Path implements INodeType{
	PATH("Path Node");

	private String name;

	private Path(String s) {

		this.name = s;
	}

	@Override
	public String toString() {
		return this.name;
	}

}
