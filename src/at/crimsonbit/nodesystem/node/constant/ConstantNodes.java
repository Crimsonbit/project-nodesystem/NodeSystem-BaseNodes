package at.crimsonbit.nodesystem.node.constant;

import at.crimsonbit.nodesystem.nodebackend.INodeType;

/**
 * 
 * Collection of all Constant Node Types
 * 
 * @author Alexander Daum, Florian Wagner
 *
 */
public enum ConstantNodes implements INodeType {

	STRING_CONSTANT("String Constant"), INTEGER_CONSTANT("Integer Constant"), DOUBLE_CONSTANT("Double Constant"),
	FLOAT_CONSTANT("Float Constant"), LONG_CONSTANT("Long Constant"), CHAR_CONSTANT("Char Constant"),
	BOOLEAN_CONSTANT("Boolean Constant"), SHORT_CONSTANT("Short Constant"), BYTE_CONSTANT("Byte Constant");

	private String name;

	private ConstantNodes(String s) {
		this.name = s;
	}

	@Override
	public String nodeName() {
		return this.name;
	}
}
