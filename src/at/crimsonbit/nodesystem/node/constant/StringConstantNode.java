package at.crimsonbit.nodesystem.node.constant;

import at.crimsonbit.nodesystem.nodebackend.annotations.NodeField;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeOutput;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;

public class StringConstantNode extends AbstractNode {

	@NodeField
	@NodeOutput("compute")
	String constant;

	@Override
	public void compute() {
	}

}
