package at.crimsonbit.nodesystem.node.constant;

import at.crimsonbit.nodesystem.nodebackend.annotations.NodeField;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeOutput;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;

public class LongConstantNode extends AbstractNode {

	@NodeField
	@NodeOutput("compute")
	long constant;

	@Override
	public void compute() {
	}

}
