package at.crimsonbit.nodesystem.node.constant;

import at.crimsonbit.nodesystem.nodebackend.annotations.NodeField;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeOutput;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;

public class DoubleConstantNode extends AbstractNode {

	@NodeField
	@NodeOutput("compute")
	double constant;

	@Override
	public void compute() {
	}

}
