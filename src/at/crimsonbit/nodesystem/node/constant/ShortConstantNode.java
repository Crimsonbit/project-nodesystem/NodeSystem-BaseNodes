package at.crimsonbit.nodesystem.node.constant;

import at.crimsonbit.nodesystem.nodebackend.annotations.NodeField;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeOutput;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;

public class ShortConstantNode extends AbstractNode {

	@NodeField
	@NodeOutput("compute")
	short constant;

	@Override
	public void compute() {
	}

}
